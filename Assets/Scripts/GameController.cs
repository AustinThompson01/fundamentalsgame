﻿//This Script is inspired by Game Dev Experiments on YouTube, Pause menu is my own
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState { FreeRoam, Battle, Pause }

public class GameController : MonoBehaviour
{
    [SerializeField] PlayerMovement playerController;
    [SerializeField] BattleSystem battleSystem;
    [SerializeField] PauseMenu pauseMenu;
    [SerializeField] Unit unit;

    [SerializeField] Camera pauseCamera;
    [SerializeField] Camera worldCamera;

    GameState state;
    GameState prevState;

    private void Start()
    {
        playerController.OnEncounter += StartBattle;
        pauseMenu.PauseGame += Pause;
        pauseMenu.UnPauseGame += UnPause;
        battleSystem.OnBattleOver += EndBattle;
        unit.IsStrong += WinGame;
    }

    void WinGame()
    {
        SceneManager.LoadScene("WinScene");
    }

    void StartBattle()
    {
        state = GameState.Battle;
        battleSystem.gameObject.SetActive(true);
        worldCamera.gameObject.SetActive(false);
        pauseCamera.gameObject.SetActive(false);

        var playerParty = playerController.GetComponent<PlayerParty>();
        var enemyUnit = FindObjectOfType<MapArea>().GetComponent<MapArea>().GetRandomEnemyUnit();

        battleSystem.StartBattle(playerParty, enemyUnit);
    }

    void Pause()
    {
        if (state != GameState.Battle)
        {
            prevState = state;
            state = GameState.Pause;
            battleSystem.gameObject.SetActive(false);
            worldCamera.gameObject.SetActive(false);
            pauseCamera.gameObject.SetActive(true);
        }
    }

    void UnPause()
    {
        state = prevState;
        pauseCamera.gameObject.SetActive(false);
    }

    void EndBattle(bool won, bool ran)
    {
        state = GameState.FreeRoam;
        battleSystem.gameObject.SetActive(false);
        worldCamera.gameObject.SetActive(true);
        pauseCamera.gameObject.SetActive(false);

        if(!won)
        {
            SceneManager.LoadScene("LoseScene");
        }else if(won && !ran)
        {
            unit.LevUp();
        }
    }

    private void Update()
    {
        if(state == GameState.FreeRoam)
        {
            worldCamera.gameObject.SetActive(true);
            playerController.HandleUpdate();
        }
        else if(state == GameState.Battle)
        {
            battleSystem.gameObject.SetActive(true);
            battleSystem.HandleUpdate();
        }
        else if(state == GameState.Pause)
        {
            pauseMenu.HandleUpdate();
        }
    }
}
