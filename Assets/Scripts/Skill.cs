﻿/*This script is created by Game Dev Experiments tutorial series that can be found in the readme
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill
{
    public BaseSkill Base { get; set; }

    public Skill(BaseSkill sBase)
    {
        Base = sBase;
    }
}
