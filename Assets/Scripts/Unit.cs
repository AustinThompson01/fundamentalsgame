﻿/*This script is inpsired by Game Dev Experiments tutorial series that can be found in the readme
 */
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Unit
{
    [SerializeField] BaseUnit _base;
    [SerializeField] int level;

    public event Action IsStrong;

    public BaseUnit Base {
        get
        {
            return _base;
        }
    }
    public int Level { 
        get
        {
            return level;
        }
    }

    public int HP { get; set; }
    public int TP { get; set; }

    public List<Skill> Skills { get; set; }

    public void Init()
    {
        HP = MaxHP;
        TP = MaxTP;
        //Generates skills
        Skills = new List<Skill>();
        foreach(var skill in Base.LearnableSkills)
        {
            if(skill.Level <= Level)
            {
                Skills.Add(new Skill(skill.Base));
            }
        }
    }

    public void LevUp()
    {
        level++;
        if (level == 10)
            IsStrong();
    }

    public int Attack
    {
        get { return Mathf.FloorToInt((Base.Attack * Level) / 10f) + Level; }
    }

    public int Defense
    {
        get { return Mathf.FloorToInt((Base.Defense * Level) / 10f) + Level; }
    }

    public int MaxHP
    {
        get { return Mathf.FloorToInt((Base.MaxHP * Level) / 10f) + Level * 2 + 6; }
    }

    public int MaxTP 
    { 
        get { return Base.MaxTP; }
    }
    

    public bool TakeDamage(Unit attacker)
    {
        int damage = Mathf.FloorToInt(attacker.Attack - Defense);
        if (damage <= 0)
            damage = 1;

        HP -= damage;
        if(HP <= 0)
        {
            HP = 0;
            return true;
        }

        return false;
    }

    public bool TakeDamage(Skill skill, Unit attacker)
    {
        float x = skill.Base.Damage;
        float y = skill.Base.Hitnum;

        int damage = 0;

        for (float i = 0; i < y; i += 1)
        {
            if (Mathf.FloorToInt((x + attacker.Attack - Defense)) > 0)
            {
                damage += Mathf.FloorToInt((x + attacker.Attack - Defense));
            }
            else if(Mathf.FloorToInt((x + attacker.Attack - Defense)) < 0)
            {
                damage += 1;
            }
        }

        HP -= damage;

        if(HP <= 0)
        {
            HP = 0;
            return true;
        }

        return false;
    }

    public Skill GetRandomSkill()
    {
        int r = UnityEngine.Random.Range(0, Skills.Count);
        if (Skills[r] != null)
        {
            return Skills[r];
        }
        else
        {
            return Skills[0];
        }
    }
}
