﻿//This script was made using Game Dev Experiment's videos on youtube Credit goes to him
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleUnit : MonoBehaviour
{
    [SerializeField] bool isPlayerUnit;

    public Unit Unit { get; set; }

    public void Setup(Unit unit)
    {
        Unit = unit;
        if (isPlayerUnit)
        {
            GetComponent<Image>().sprite = Unit.Base.BackSprite;
        }
        else
        {
            GetComponent<Image>().sprite = Unit.Base.FrontSprite;
        }
    }
}
