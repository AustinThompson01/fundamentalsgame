﻿/*This code is not my own, it is by Game Dev Experiments on YouTube and any piece of movement related code
 * even if not done by him was inspired by him as I have learned how to do so by his videos.
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public LayerMask solidObjectsLayer;
    public LayerMask EncounterObjectsLayer;

    public event Action OnEncounter;

    private bool isMoving;
    private Vector2 input;

    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void HandleUpdate()
    {
        if (!isMoving)
        {
            input.x = Input.GetAxisRaw("Horizontal");
            input.y = Input.GetAxisRaw("Vertical");

            if (input.x != 0)   //prevents Diagonal movement
                input.y = 0;

            if (input != Vector2.zero)
            {
                animator.SetFloat("moveX", input.x);
                animator.SetFloat("moveY", input.y);    //sets animations

                var targetPos = transform.position;
                targetPos.x += input.x;
                targetPos.y += input.y;

                if (CanMove(targetPos))
                {
                    Encounter(targetPos);
                    StartCoroutine(Move(targetPos));
                }
            }
        }

        animator.SetBool("isMoving", isMoving); //sets animation to moving or idle branch
    }

    IEnumerator Move(Vector3 targetPos)
    {
        isMoving = true;    //only plays once

        while((targetPos - transform.position).sqrMagnitude > Mathf.Epsilon)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);    //does the moving
            yield return null;
        }
        transform.position = targetPos;
        isMoving = false;
    }

    private bool CanMove(Vector3 targetPos)
    {
        if (Physics2D.OverlapCircle(targetPos, 0.3f, solidObjectsLayer) != null)
        {
            return false;
        }
        return true;
    }

    public void Encounter(Vector3 targetPos)
    {
        if (Physics2D.OverlapCircle(targetPos, 0.3f, EncounterObjectsLayer) != null)
        {
            if (UnityEngine.Random.Range(1, 101) <= 10)
            {
                animator.SetBool("isMoving", false);
                OnEncounter();
            }
        }
    }
}
