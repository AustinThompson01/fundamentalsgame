﻿/*This script is inpsired by Game Dev Experiments tutorial series that can be found in the readme
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Unit", menuName = "Unit/Create new Unit")]
public class BaseUnit : ScriptableObject
{
    [SerializeField] string name;

    [SerializeField] Sprite frontSprite;
    [SerializeField] Sprite backSprite;

    [SerializeField] int maxHP;
    [SerializeField] int attack;
    [SerializeField] int defense;
    [SerializeField] int maxTP;

    [SerializeField] List<LearnableSkill> learnableSkills;

    public string Name
    {
        get { return name; }
    }

    public Sprite FrontSprite
    {
        get { return frontSprite; }
    }

    public Sprite BackSprite
    {
        get { return backSprite; }
    }

    public int MaxHP
    {
        get { return maxHP; }
    }

    public int Attack
    {
        get { return attack; }
    }

    public int Defense
    {
        get { return defense; }
    }

    public int MaxTP
    {
        get { return maxTP; }
    }

    public List<LearnableSkill> LearnableSkills
    {
        get { return learnableSkills; }
    }
}


[System.Serializable]
public class LearnableSkill
{
    [SerializeField] BaseSkill baseSkill;
    [SerializeField] int level;

    public BaseSkill Base
    {
        get { return baseSkill; }
    }

    public int Level
    {
        get { return level; }
    }
}