﻿//This script was made using Game Dev Experiments video on YouTube
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleHUD : MonoBehaviour
{
    [SerializeField] Text nameText;
    [SerializeField] Text levelText;
    [SerializeField] Text hpText;
    [SerializeField] HPBar hpBar;

    [SerializeField] Color tpColor;
    [SerializeField] List<Image> tpImage;

    Unit _unit;

    public void SetData(Unit unit)
    {
        for (int i = 0; i < tpImage.Count; i++)
        {
            if (i < unit.TP)
            {
                tpImage[i].GetComponent<Image>().enabled = true;
            }
            else
            {
                tpImage[i].GetComponent<Image>().enabled = false;
            }
        }

        _unit = unit;

        nameText.text = unit.Base.Name;
        levelText.text = "LV: " + unit.Level;
        hpText.text = "HP: " + unit.HP;
        hpBar.SetHP((float)unit.HP / unit.MaxHP);
    }

    public IEnumerator UpdateHP()
    {
        yield return hpBar.SetHPSmooth((float)_unit.HP / _unit.MaxHP);
        
        hpText.text = "HP: " + _unit.HP;

        for (int i = 0; i < tpImage.Count; i++)
        {
            if (i < _unit.TP)
            {
                tpImage[i].GetComponent<Image>().enabled = true;
            }
            else
            {
                tpImage[i].GetComponent<Image>().enabled = false;
            }
        }
    }
}
