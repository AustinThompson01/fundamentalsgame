﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] List<Text> pauseText;
    [SerializeField] Color highlight;

    private int selection = 0;

    public event Action UnPauseGame;
    public event Action PauseGame;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
    }

    public void HandleUpdate()
    {
        if(Input.GetKeyDown(KeyCode.W)|| Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (selection < 1)
                selection++;
        }
        else if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (selection > 0)
                selection--;
        }

        for (int i = 0; i < pauseText.Count; ++i)
        {
            if (i == selection)
                pauseText[i].color = highlight;
            else
                pauseText[i].color = Color.white;
        }

        if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Space))
        {
            if(selection == 0)
            {
                Debug.Log("Quit Game");
                Application.Quit();
            }
            else if(selection == 1)
            {
                UnPauseGame();
            }
        }
    }
}
