﻿//This script was a mix of Game Dev Experiments tutorial and my own coding after seeing how everything interacts with one another
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BattleState { Start, PlayerTurn, PlayerMove, EnemyMove, Busy}

public class BattleSystem : MonoBehaviour
{
    [SerializeField] BattleUnit playerUnit;
    [SerializeField] BattleUnit enemyUnit;
    [SerializeField] BattleHUD playerHud;
    [SerializeField] BattleHUD enemyHud;
    [SerializeField] BattleDialogBox dialogBox;

    public event Action<bool, bool> OnBattleOver;

    private AudioSource audioData;

    BattleState state;
    int currentAction = 0;
    int currentMove = 0;

    PlayerParty playerParty;
    Unit enemyUnits;

    public void StartBattle(PlayerParty playerParty, Unit enemyUnits)
    {
        audioData = GetComponent<AudioSource>();
        this.playerParty = playerParty;
        this.enemyUnits = enemyUnits;
        StartCoroutine(SetupBattle());
    }

    public IEnumerator SetupBattle()        //This sets up the battle
    {
        audioData.Play(0);
        playerUnit.Setup(playerParty.GetHealthyUnit());
        enemyUnit.Setup(enemyUnits);
        playerHud.SetData(playerUnit.Unit);
        enemyHud.SetData(enemyUnit.Unit);

        dialogBox.SetMoveNames(playerUnit.Unit.Skills);

        yield return dialogBox.TypeDialog($"A {enemyUnit.Unit.Base.Name} appeared.");
        yield return new WaitForSeconds(3f);

        PlayerTurn();
    }

    void PlayerTurn()       //This is what plays during the player's turn allowing you to choose an action
    {
        state = BattleState.PlayerTurn;
        StartCoroutine(dialogBox.TypeDialog("Choose an action"));
        dialogBox.EnableActionSelector(true);
    }

    void PlayerMove()       //This is the action performed if the player selects "Skills"
    {
        state = BattleState.PlayerMove;
        dialogBox.EnableActionSelector(false);
        dialogBox.EnableDialogText(false);
        dialogBox.EnableSkillSelector(true);
    }

    IEnumerator PerformPlayerMove()     //This is the action performed if the player selects "Skills"
    {
        state = BattleState.Busy;

        var move = playerUnit.Unit.Skills[currentMove];

        if (playerUnit.Unit.TP >= move.Base.TP)     //Checks if you have enoug TP for move 
        {
            yield return dialogBox.TypeDialog($"{playerUnit.Unit.Base.Name} used {move.Base.Name}");

            yield return new WaitForSeconds(2f);

            bool isDead = enemyUnit.Unit.TakeDamage(move, playerUnit.Unit);
            yield return enemyHud.UpdateHP();

            playerUnit.Unit.TP -= move.Base.TP;
            yield return playerHud.UpdateHP();

            yield return new WaitForSeconds(1f);

            if (isDead)
            {
                yield return dialogBox.TypeDialog($"{enemyUnit.Unit.Base.Name} has died");

                yield return new WaitForSeconds(3f);
                if(playerUnit.Unit.Level < 9)
                    playerUnit.Unit.LevUp();
                audioData.Stop();
                OnBattleOver(true, false);
            }
            else
            {
                yield return new WaitForSeconds(1f);
                StartCoroutine(EnemyMove());
            }
        }
        else
        {
            yield return dialogBox.TypeDialog($"{playerUnit.Unit.Base.Name} does not have enoug TP for {move.Base.Name}");
            yield return new WaitForSeconds(3f);
            yield return dialogBox.TypeDialog($"{playerUnit.Unit.Base.Name} Will attack instead");
            yield return new WaitForSeconds(2f);
            StartCoroutine(PerformPlayerAttack());
        }
    }

    void PlayerAttack()     //This is the acction performed if the player selects "attack"
    {
        state = BattleState.Busy;
        dialogBox.EnableActionSelector(false);
        dialogBox.EnableDialogText(true);
        dialogBox.EnableSkillSelector(false);
        StartCoroutine(PerformPlayerAttack());
    }

    IEnumerator PerformPlayerAttack()   //This is the action performed if the player selects "attack"
    {
        yield return dialogBox.TypeDialog($"{playerUnit.Unit.Base.Name} Attacked {enemyUnit.Unit.Base.Name}");

        if (playerUnit.Unit.TP < playerUnit.Unit.Base.MaxTP)
            playerUnit.Unit.TP += 1;

        yield return new WaitForSeconds(3f);

        bool isDead = enemyUnit.Unit.TakeDamage(playerUnit.Unit);
        yield return enemyHud.UpdateHP();
        yield return playerHud.UpdateHP();
        if (isDead)
        {
            yield return dialogBox.TypeDialog($"{enemyUnit.Unit.Base.Name} has died");
            if (playerUnit.Unit.Level < 9)
                playerUnit.Unit.LevUp();
            yield return new WaitForSeconds(3f);
            audioData.Stop();
            OnBattleOver(true, false);
        }
        else
        {
            yield return new WaitForSeconds(1f);
            StartCoroutine(EnemyMove());
        }
    }

    IEnumerator EnemyMove()     //This is the enemy move
    {
        state = BattleState.EnemyMove;

        var move = enemyUnit.Unit.GetRandomSkill();
        bool isDead = false;


        if (enemyUnit.Unit.TP >= move.Base.TP)  //Checks if the enemy has the TP to use move
        {
            yield return dialogBox.TypeDialog($"{enemyUnit.Unit.Base.Name} used {move.Base.Name}");

            yield return new WaitForSeconds(3f);
            
            enemyUnit.Unit.TP -= move.Base.TP;
            
            isDead = playerUnit.Unit.TakeDamage(move, playerUnit.Unit);
        }
        else
        {
            enemyUnit.Unit.TP += 1;

            yield return dialogBox.TypeDialog($"{enemyUnit.Unit.Base.Name} attacks! ");

            yield return new WaitForSeconds(3f);

            isDead = playerUnit.Unit.TakeDamage(enemyUnit.Unit);
        }


        yield return playerHud.UpdateHP();
        yield return enemyHud.UpdateHP();

        if (isDead)
        {
            yield return dialogBox.TypeDialog($"{playerUnit.Unit.Base.Name} has died");

            yield return new WaitForSeconds(3f);
            OnBattleOver(false, false);
        }
        else
        {
            PlayerTurn();
        }
    }

    public void HandleUpdate()   //This is the update which will help handle the game states
    {
        if(state == BattleState.PlayerTurn)
        {
            HandleActionSelection();
        }
        else if (state == BattleState.PlayerMove)
        {
            HandleMoveSelection();
        }
    }

    void HandleActionSelection()    //Selections Action using arrows and WASD
    {
        if (Input.GetKeyDown(KeyCode.DownArrow)||Input.GetKeyDown(KeyCode.S))
        {
            if (currentAction < 2)
                ++currentAction;
        }else if (Input.GetKeyDown(KeyCode.UpArrow)||Input.GetKeyDown(KeyCode.W))
        {
            if (currentAction > 0)
                --currentAction;
        }

        dialogBox.UpdateActionSelection(currentAction);

        if(Input.GetKeyDown(KeyCode.Z)|| Input.GetKeyDown(KeyCode.Space))
        { 
            if(currentAction == 0)  //attack
            {
                PlayerAttack();
            }else if(currentAction == 1) //Skill
            {
                PlayerMove();
            }else if(currentAction == 2) //Run
            {
                dialogBox.EnableActionSelector(false);
                state = BattleState.Busy;
                StartCoroutine(Escape());
            }
        }
    }

    IEnumerator Escape()
    {
        yield return dialogBox.TypeDialog($"Ran Away");
        yield return new WaitForSeconds(1f);
        audioData.Stop();
        OnBattleOver(true, true);
    }

    void HandleMoveSelection()  //Selects move using arrows and WASD
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            if (currentMove < playerUnit.Unit.Skills.Count - 1)
                ++currentMove;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            if (currentMove > 0)
                --currentMove;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            if (currentMove < playerUnit.Unit.Skills.Count - 2)
                currentMove += 2;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            if (currentMove > 1)
                currentMove -= 2;
        }

        dialogBox.UpdateMoveSelection(currentMove, playerUnit.Unit.Skills[currentMove]);

        if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Space))
        {
            dialogBox.EnableSkillSelector(false);
            dialogBox.EnableDialogText(true);
            StartCoroutine(PerformPlayerMove());
        }
    }

}
