﻿//This script was made by using Game Dev Experiments tutorial on YouTube
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerParty : MonoBehaviour
{
    [SerializeField] List<Unit> units;

    private void Start()
    {
        foreach(var unit in units)
        {
            unit.Init();
        }
    }

    public Unit GetHealthyUnit()
    {
        foreach(var unit in units)
        {
            unit.Init();
        }
        return units.Where(x => x.HP > 0).FirstOrDefault();
    }
}
