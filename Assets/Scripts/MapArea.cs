﻿//This script was made by following Game Dev Experiments on YouTube's tutorial
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapArea : MonoBehaviour
{
    [SerializeField] List<Unit> enemyUnits;

    public Unit GetRandomEnemyUnit()
    {
        var enemyUnit = enemyUnits[Random.Range(0, enemyUnits.Count)];
        enemyUnit.Init();
        return enemyUnit;
    }
}
