﻿/*This script is inpsired by Game Dev Experiments tutorial series that can be found in the readme
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Skill", menuName = "Unit/Create new skill")]
public class BaseSkill : ScriptableObject
{
    [SerializeField] string name;

    [SerializeField] int damage;
    [SerializeField] int hitnum;
    [SerializeField] int tp;
    
    public string Name
    {
        get { return name; }
    }

    public int Damage
    {
        get { return damage; }
    }

    public int Hitnum
    {
        get { return hitnum; }
    }

    public int TP
    {
        get { return tp; }
    }
}
