Thank you user ArMM1998 for the sprites and tilesets. Downloaded from https://opengameart.org/content/zelda-like-tilesets-and-sprites

Thank you user ansimuz for the battle background, this is the background that appears in all battles and requires that I attribute credit to ansimuz and link where I got it from. 
https://opengameart.org/content/battle-background-hazy-hills-0 I also must specify if I made changes, in the battle scenes I put in my own menu I do not know if this must be mentioned
but for legal reasons I will anyway.

Thank you user Metaruka for the music titled "01 game-game_0" which plays in battles and "1x game over" which plays in game overs.
https://opengameart.org/content/game-game

Thank you user Gigawowski for the music titled "Menu_Town" which plays if you win the game. 
https://opengameart.org/content/menutown-music

The PlayerMovement code was written by Game Dev Experiments on YouTube. The link for the video is here https://www.youtube.com/watch?v=_Pm16a18zy8&t=13s&ab_channel=GameDevExperiments
The BaseSkill and BaseUnit scripts were inspired by Game Dev Experiments on YouTube, it should be found in the play list linked previously. I changed a bit to make the system I wanted
The Skill and Unit scripts were inspired by Game Dev Experiments on YouTube, I modified both to check for TP in battle (TP is a resource you gain by attacking normally)
The PlayerParty script was made by Game Dev Experiments on YouTube, though you only play 1 player a designer could easily make more.
The MapArea script was made by Game Dev Experiments on Youtube.
The HPbar, BattleDialog ,and BattleUnit were made by Game Dev Experiments on YouTube.
The BattleSystem, GameController, and BattleHUD were all modified from what Game Dev Experiments did to better suit my needs.
The PauseMenu, MainMenu, and WinTimer(which should be renamed but I'm lazy) are entierly my own. Though the PauseMenu uses other scripts such as the GameController which Game Dev Experiments did most of.
All the animations except the playerwin animations were made according to how Game Dev Experiments did so.

I feel as though throughout this all I have learned a lot, specifically about OOP. Typically in unity tutorials or even in classes
we are taught about OOP but never are we shown how to implement it or how it actually helps. To have seen it used and now have used
it practically has made me a far better programmer even if a lot of this code is sloppy. This also helped me learn some of how to use unity animator, though GSW helped me far more in that regard.
The around 6 weeks I had to make this after restarting having realized where one ends-up without a solid code foundation has been the best weeks for my education in my entire college carrer.
The headaches and all were worth it, and to be frank given that this is the result of 6 weeks with other looming deadlines over my head despite the clearly
rough edges and clearly rushed production it is something that I am the most proud of in my college carrer.

Anyway let's hope I pass Linear Algebra, because I spent a lot of time not studying it.